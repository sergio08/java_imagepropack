package utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.io.File;


public class ImageUtils {

    private static ImageUtils ourInstance = new ImageUtils();

    public static ImageUtils getInstance() {
        return ourInstance;
    }

    private ImageUtils() {
    }

    /**
     * Read and load the image
     **/
    public static BufferedImage loadImageFromPath(String path) throws IOException {
        return ImageIO.read(new File(path));
    }

    /**
     * Write buffer image to jpg
     */
    public static void writeImageToPath(BufferedImage inputFile, String path) throws IOException{
        File outputFile = new File(path);
        ImageIO.write(inputFile, "png", outputFile);
    }


    /**
     *  Returns image metadata
     **/

    public static String getInfo(BufferedImage image) {
        int pixelsQty = ((DataBufferByte) image.getRaster().getDataBuffer()).getData().length;
        int width = image.getWidth();
        int height = image.getHeight();
        int channels = pixelsQty / (width * height);
        return String.format("Image_type : %s, total_pixels : %s, width : %s, height : %s, channels : %s", image.getType(), pixelsQty, width, height, channels);
    }

    /**
     * Returns grayscale image version
     * adapted from http://codehustler.org/blog/java-to-create-grayscale-images-icons/
     * adapted from
     **/

    public static BufferedImage getGrayscale_alt1(BufferedImage image){
        BufferedImage grayImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics graphic = grayImage.createGraphics();
        graphic.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        graphic.dispose();
        return grayImage;
    }

    /**
     * Returns grayscale image version
     **/

    public static BufferedImage getGrayscale_alt2(BufferedImage image){
        BufferedImage grayImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        Graphics graphic = grayImage.createGraphics();
        graphic.drawImage(image, 0, 0, null);
        graphic.dispose();
        return grayImage;
    }

    /**
     * Returns grayscale image version  - manual
     * adapted from https://stackoverflow.com/questions/9131678/convert-a-rgb-image-to-grayscale-image-reducing-the-memory-in-java
     **/

    public static BufferedImage getGrayscale_alt3(BufferedImage image) {        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                int rgb = image.getRGB(x, y);
                int r = (rgb >> 16) & 0xFF;
                int g = (rgb >> 8) & 0xFF;
                int b = (rgb & 0xFF);

                // Normalize and gamma correct:
                float rr = (float) Math.pow(r / 255.0, 2.2);
                float gg = (float) Math.pow(g / 255.0, 2.2);
                float bb = (float) Math.pow(b / 255.0, 2.2);

                // Calculate luminance:
                float lum = (float) (0.2126 * rr + 0.7152 * gg + 0.0722 * bb);

                // Gamma compand and rescale to byte range:
                int grayLevel = (int) (255.0 * Math.pow(lum, 1.0 / 2.2));
                int gray = (grayLevel << 16) + (grayLevel << 8) + grayLevel;
                image.setRGB(x, y, gray);
            }
        }
        return image ;
    }


        /**
         * Get the inversed image
         **/

        public static BufferedImage getInverse(BufferedImage image) {
            int width = image.getWidth();
            int height = image.getHeight();
            //System.out.println(width);
            //System.out.println(height);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    int rgba = image.getRGB(x, y);
                    Color col = new Color(rgba, true);
                    col = new Color(255 - col.getRed(),
                            255 - col.getGreen(),
                            255 - col.getBlue());
                    image.setRGB(x, y, col.getRGB());
                }
            }
            return image;
        }



    /**
     * Get the inversed image
     **/
    public static int[] getGrayHist(BufferedImage image) {
        int[] h = new int[256];
        int width = image.getWidth();
        int height = image.getHeight();
        //System.out.println(width);
        //System.out.println(height);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int px = image.getRGB(x,y) >> 8 & 0xFF;
                h[px] = h[px] + 1;
            }
        }
        return h;
    }



}

