package utils;

import org.junit.Assert;
import org.junit.Test;


import java.io.IOException;
import java.awt.image.BufferedImage;
import java.util.Arrays;

public class TestImageUtils {

    private ImageUtils imageUtils = ImageUtils.getInstance();

    public String getResourcePath(String resourceName) {
        return getClass().getClassLoader().getResource(resourceName).getFile();
    }

    public String getExpectedString(int imageType, int totalPixels, int width, int height, int channels) {
        return String.format("Image_type : %s, total_pixels : %s, width : %s, height : %s, channels : %s",
                imageType, totalPixels, width, height, channels);
    }

    @Test
    public void testLoadImage() throws IOException {
        String path = getResourcePath("lena.jpg");
        BufferedImage buffImage = imageUtils.loadImageFromPath(path);
        Assert.assertNotNull(buffImage);
    }

    @Test
    public void testInvertImage() throws IOException {
        String path = getResourcePath("town.jpg");
        BufferedImage image = imageUtils.loadImageFromPath(path);
        System.out.println(ImageUtils.getInfo(image));
        //String outpath = getResourcePath("inverted_lena.jpg");
        String outpath = path.substring(0, path.lastIndexOf('.')) + "_INVERTED.jpg";
        //System.out.print(outpath);
        long startTime = System.currentTimeMillis();
        ImageUtils.writeImageToPath( ImageUtils.getInverse(image) , outpath  );
        long endTime = System.currentTimeMillis();
        System.out.println("That took " + (endTime - startTime) + " milliseconds");
        BufferedImage invimage = imageUtils.loadImageFromPath(outpath);
        System.out.println(ImageUtils.getInfo(invimage));
        Assert.assertNotNull(invimage);
    }

    @Test
    public void testGrayImage() throws IOException {
        String path = getResourcePath("sea.jpg");
        BufferedImage image = imageUtils.loadImageFromPath(path);
        System.out.println(ImageUtils.getInfo(image));
        String outpath = path.substring(0, path.lastIndexOf('.')) + "_GRAY1.jpg";
        long startTime = System.currentTimeMillis();
        ImageUtils.writeImageToPath( ImageUtils.getGrayscale_alt1(image) , outpath  );
        long endTime = System.currentTimeMillis();
        System.out.println("Gray convertion took:  " + (endTime - startTime) + " milliseconds");
        BufferedImage invimage = imageUtils.loadImageFromPath(outpath);
        System.out.println(ImageUtils.getInfo(invimage));
        Assert.assertNotNull(invimage);
    }


    @Test
    public void getGrayHist() throws IOException {
        String path = getResourcePath("sea.jpg");
        BufferedImage image = imageUtils.loadImageFromPath(path);
        System.out.println(ImageUtils.getInfo(image));
        BufferedImage grayimage = ImageUtils.getGrayscale_alt1(image);
        System.out.println(Arrays.toString(ImageUtils.getGrayHist(grayimage)));
        Assert.assertNotNull(grayimage);
    }

}
